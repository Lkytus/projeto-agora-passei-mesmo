package net.uva.trabalhoa1.classes;

public final class Motor {
    private String marca, modelo, tipoCombustivel;
    private int potencia, rpmMaxima, rpm;

    public Motor() {
    }

    public Motor(String marca) {
	cadastrar(marca);
    }

    public Motor(String marca, String modelo) {
	cadastrar(marca, modelo);
    }

    public Motor(String marca, String modelo, String tipoCombustivel) {
	cadastrar(marca, modelo, tipoCombustivel);
    }

    public Motor(String marca, String modelo, String tipoCombustivel, int potencia) {
	cadastrar(marca, modelo, tipoCombustivel, potencia);
    }

    public Motor(String marca, String modelo, String tipoCombustivel, int potencia, int rpmMaxima) {
	cadastrar(marca, modelo, tipoCombustivel, potencia, rpmMaxima);
    }

    public Motor(String marca, String modelo, String tipoCombustivel, int potencia, int rpmMaxima, int rpm) {
	cadastrar(marca, modelo, tipoCombustivel, potencia, rpmMaxima, rpm);
    }
    
    public void cadastrar(String marca) {
        this.marca = marca;
    }
    
    public void cadastrar(String marca, String modelo) {
        cadastrar(marca);
        this.modelo = modelo;
    }
    
    public void cadastrar(String marca, String modelo, String tipoCombustivel) {
        cadastrar(marca, modelo);
        this.tipoCombustivel = tipoCombustivel;
    }
    
    public void cadastrar(String marca, String modelo, String tipoCombustivel, int potencia) {
        cadastrar(marca, modelo, tipoCombustivel);
        this.potencia = potencia;
    }
    
    public void cadastrar(String marca, String modelo, String tipoCombustivel, int potencia, int rpmMaxima) {
        cadastrar(marca, modelo, tipoCombustivel, potencia);
        this.rpmMaxima = rpmMaxima;
    }
    
    public void cadastrar(String marca, String modelo, String tipoCombustivel, int potencia, int rpmMaxima, int rpm) {
	cadastrar(marca, modelo, tipoCombustivel, potencia, rpmMaxima);
        this.rpm = rpm;
    }
    
    public void imprimir() {
	System.out.println("Marca: " + marca);
	System.out.println("Modelo: " + modelo);
	System.out.println("Tipo de Combustivel: " + tipoCombustivel);
	System.out.println("Potencia: " + potencia);
	System.out.println("RPM Maxima: " + rpmMaxima);
	System.out.println("RPM: " + rpm);
    }
    
    public void entradaDados() {
	System.out.print("Marca: "); marca = Config.IN_READ.nextLine();
	System.out.print("Modelo: "); modelo = Config.IN_READ.nextLine();
	System.out.print("Tipo de Combustivel: "); tipoCombustivel = Config.IN_READ.nextLine();
	System.out.print("Potencia: "); potencia = Integer.parseInt(Config.IN_READ.nextLine());
	System.out.print("RPM Maxima: "); rpmMaxima = Integer.parseInt(Config.IN_READ.nextLine());
	System.out.print("RPM: "); rpm = Integer.parseInt(Config.IN_READ.nextLine());
    }
    
    public void acelerar() {
	rpm += 100;
	if(rpm > rpmMaxima) rpm = rpmMaxima;
    }
    
    public void desacelerar() {
	rpm -= 100;
	if(rpm < 0) rpm = 0;
    }
    
    public void setMarca(String marca) {
	this.marca = marca;
    }

    public void setModelo(String modelo) {
	this.modelo = modelo;
    }

    public void setTipoCombustivel(String tipoCombustivel) {
	this.tipoCombustivel = tipoCombustivel;
    }

    public void setPotencia(int potencia) {
	this.potencia = potencia;
    }

    public void setRpmMaxima(int rpmMaxima) {
	this.rpmMaxima = rpmMaxima;
    }

    public void setRpm(int rpm) {
	this.rpm = rpm;
    }
    
    public String getMarca() {
	return marca;
    }

    public String getModelo() {
	return modelo;
    }

    public String getTipoCombustivel() {
	return tipoCombustivel;
    }

    public int getPotencia() {
	return potencia;
    }

    public int getRpmMaxima() {
	return rpmMaxima;
    }

    public int getRpm() {
	return rpm;
    }
    
    
}
