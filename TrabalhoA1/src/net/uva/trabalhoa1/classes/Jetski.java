package net.uva.trabalhoa1.classes;

public final class Jetski extends Veiculo {
    
    private boolean reboque;

    public Jetski() {
    }

    public Jetski(Motor motor1) {
	super(motor1);
    }

    public Jetski(Motor motor1, String marca) {
	super(motor1, marca);
    }

    public Jetski(Motor motor1, String marca, String modelo) {
	super(motor1, marca, modelo);
    }

    public Jetski(Motor motor1, String marca, String modelo, String tipoCasco) {
	super(motor1, marca, modelo, tipoCasco);
    }

    public Jetski(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao) {
	super(motor1, marca, modelo, tipoCasco, identificacao);
    }

    public Jetski(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima);
    }

    public Jetski(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco);
    }

    public Jetski(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
    }

    public Jetski(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, boolean reboque) {
        cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros, reboque);
    }
    
    public void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, boolean reboque) {
	super.cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
	this.reboque = reboque;
    }
    
    @Override
    public void imprimir() {
	super.imprimir();
	System.out.println ("Reboque: " + (reboque ? "Sim" : "Nao") );
    }
    
    @Override
    public void entradaDados() {
	super.entradaDados();
	reboque = Config.inReadNextResultBasedOnValuePreset("Reboque(s/n): ", "s", "n");
    }
    
    public boolean isReboque() {
	return reboque;
    }

    public void setReboque(boolean reboque) {
	this.reboque = reboque;
    }
    
    @Override
    public double valorDesconto() {
	return preco * 0.85;
    }
    
}
