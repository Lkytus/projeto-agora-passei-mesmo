package net.uva.trabalhoa1.classes;

public abstract class Veiculo {
    protected Motor motor1;
    
    protected String marca;
    protected String modelo;
    protected String tipoCasco;
    
    protected String identificacao;
    protected int velocidadeMaxima;
    
    protected double preco;
    protected int numeroPassageiros;

    public Veiculo() {
	motor1 = new Motor(); //para nao dar problema.
    }

    public Veiculo(Motor motor1) {
        cadastrar(motor1);
    }

    public Veiculo(Motor motor1, String marca) {
        cadastrar(motor1, marca);
    }

    public Veiculo(Motor motor1, String marca, String modelo) {
        cadastrar(motor1, marca, modelo);
    }

    public Veiculo(Motor motor1, String marca, String modelo, String tipoCasco) {
        cadastrar(motor1, marca, modelo, tipoCasco);
    }

    public Veiculo(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao) {
        cadastrar(motor1, marca, modelo, tipoCasco, identificacao);
    }

    public Veiculo(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima) {
        cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima);
    }

    public Veiculo(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco) {
        cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco);
    }

    public Veiculo(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros) {
	cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
    }
    
    public abstract double valorDesconto();
    
    public final void cadastrar(Motor motor1) {
        this.motor1 = motor1;
    }
    public final void cadastrar(Motor motor1, String marca) {
        cadastrar(motor1);
        this.marca = marca;
    }
    public final void cadastrar(Motor motor1, String marca, String modelo) {
        cadastrar(motor1, marca);
        this.modelo = modelo;
    }
    public final void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco) {
        cadastrar(motor1, marca, modelo);
        this.tipoCasco = tipoCasco;
    }
    public final void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao) {
        cadastrar(motor1, marca, modelo, tipoCasco);
        this.identificacao = identificacao;
    }
    public final void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima) {
        cadastrar(motor1, marca, modelo, tipoCasco, identificacao);
        this.velocidadeMaxima = velocidadeMaxima;
    }
    public final void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco) {
        cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima);
        this.preco = preco;
    }
    public final void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros) {
	cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco);
        this.numeroPassageiros = numeroPassageiros;
    }
    
    public void imprimir() {
	motor1.imprimir();
	System.out.println("Marca do veiculo: " + marca);
	System.out.println("Modelo do veiculo: " + modelo);
	System.out.println("Tipo de Casco: " + tipoCasco);
	System.out.println("Identificação: " + identificacao);
	System.out.println("Velocidade Maxima: " + velocidadeMaxima);
	System.out.println("Preço: " + preco);
	System.out.println("Numero de Passageiros: " + numeroPassageiros);
    }
    
    public void entradaDados() {
	motor1.entradaDados();
	System.out.print("Marca do veiculo: "); marca = Config.IN_READ.nextLine();
	System.out.print("Modelo do veiculo: "); modelo = Config.IN_READ.nextLine();
	System.out.print("Tipo de Casco: "); tipoCasco = Config.IN_READ.nextLine();
	System.out.print("Identificação: "); identificacao = Config.IN_READ.nextLine();
	System.out.print("Velocidade Maxima: "); velocidadeMaxima = Integer.parseInt(Config.IN_READ.nextLine());
	System.out.print("Preço: "); preco = Double.parseDouble(Config.IN_READ.nextLine());
	System.out.print("Numero de Passageiros: "); numeroPassageiros = Integer.parseInt(Config.IN_READ.nextLine());
    }
    
    public void setMotor1(Motor motor1) {
	this.motor1 = motor1;
    }

    public void setMarca(String marca) {
	this.marca = marca;
    }

    public void setModelo(String modelo) {
	this.modelo = modelo;
    }

    public void setTipoCasco(String tipoCasco) {
	this.tipoCasco = tipoCasco;
    }

    public void setIdentificacao(String identificacao) {
	this.identificacao = identificacao;
    }

    public void setVelocidadeMaxima(int velocidadeMaxima) {
	this.velocidadeMaxima = velocidadeMaxima;
    }

    public void setPreco(double preco) {
	this.preco = preco;
    }

    public void setNumeroPassageiros(int numeroPassageiros) {
	this.numeroPassageiros = numeroPassageiros;
    }
    
    public Motor getMotor1() {
	return motor1;
    }

    public String getMarca() {
	return marca;
    }

    public String getModelo() {
	return modelo;
    }

    public String getTipoCasco() {
	return tipoCasco;
    }

    public String getIdentificacao() {
	return identificacao;
    }

    public int getVelocidadeMaxima() {
	return velocidadeMaxima;
    }

    public double getPreco() {
	return preco;
    }

    public int getNumeroPassageiros() {
	return numeroPassageiros;
    }
    
    
}
