package net.uva.trabalhoa1.classes;

public class Lancha extends Veiculo {
    private boolean banheiro;

    public Lancha() {
    }

    public Lancha(Motor motor1) {
	super(motor1);
    }

    public Lancha(Motor motor1, String marca) {
	super(motor1, marca);
    }

    public Lancha(Motor motor1, String marca, String modelo) {
	super(motor1, marca, modelo);
    }

    public Lancha(Motor motor1, String marca, String modelo, String tipoCasco) {
	super(motor1, marca, modelo, tipoCasco);
    }

    public Lancha(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao) {
	super(motor1, marca, modelo, tipoCasco, identificacao);
    }

    public Lancha(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima);
    }

    public Lancha(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco);
    }

    public Lancha(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
    }

    public Lancha(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, boolean banheiro) {
	cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros, banheiro);
    }
    
    @Override
    public void entradaDados() {
	super.entradaDados();
	banheiro = Config.inReadNextResultBasedOnValuePreset("Banheiro(s/n): ", "s", "n");
    }
    
    @Override
    public void imprimir() {
	super.imprimir();
	System.out.println("Banheiro: " + (banheiro ? "Sim" : "Nao"));
    }
    
    public void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, boolean banheiro) {
	super.cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
	this.banheiro = banheiro;
    }
    
    @Override
    public double valorDesconto() {
	return preco *  0.9;
    }

    public boolean isBanheiro() {
	return banheiro;
    }

    public void setBanheiro(boolean banheiro) {
	this.banheiro = banheiro;
    }
    
    
}
