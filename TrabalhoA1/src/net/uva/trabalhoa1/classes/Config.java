package net.uva.trabalhoa1.classes;

//Essa classe só vai ter metodo de classe (static)

import java.util.Scanner;

//Pra não ter que ficar toda hora escrevendo Scanner scanner = new Scanner(System.in)
//Por exemplo.

public final class Config {
    private Config() { } //tornar ele "ininstanciavel" :p
    public static final Scanner IN_READ = new Scanner(System.in);
    
    //nao importa qual programa é, sempre vai ter alguma gambiarra xD
    //na verdade isso aqui e pra reduzir o tamanho de algumas coisas
    public static boolean inReadNextResultBasedOnValuePreset(String print, String forTrue, String forFalse) {
	while(true) {
	    System.out.print(print);
	    String input = IN_READ.nextLine().toLowerCase();
	    if(input.equals(forTrue.toLowerCase())) {
		return true;
	    } else if(input.equals(forFalse.toLowerCase())) {
		return false;
	    } else {
		System.err.println("Erro de comando. As entradas diponiveis são " + forTrue + " e " + forFalse);
		//o codigo repete até ir o certo.
	    }
	}
    }
}
