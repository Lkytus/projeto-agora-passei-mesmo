package net.uva.trabalhoa1.classes;

public final class Iate extends Veiculo {
    
    private int numeroTripulantes;
    private int numeroCabines;
    
    private Motor motor2;
    
    public Iate() {
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1) {
	super(motor1);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca) {
	super(motor1, marca);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo) {
	super(motor1, marca, modelo);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco) {
	super(motor1, marca, modelo, tipoCasco);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao) {
	super(motor1, marca, modelo, tipoCasco, identificacao);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros) {
	super(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
	iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, int numeroTripulantes) {
	cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros, numeroTripulantes);
        iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, int numeroTripulantes, int numeroCabines) {
	cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros, numeroTripulantes, numeroCabines);
        iniciarMotor2ComBaseMotor1();
    }

    public Iate(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, int numeroTripulantes, int numeroCabines, Motor motor2) {
        cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros, numeroTripulantes, numeroCabines, motor2);
    }
    
    public void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, int numeroTripulantes) {
	super.cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
	this.numeroTripulantes = numeroTripulantes;
    }
    
    public void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, int numeroTripulantes, int numeroCabines) {
	super.cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
	this.numeroTripulantes = numeroTripulantes;
	this.numeroCabines = numeroCabines;
    }
    
    public void cadastrar(Motor motor1, String marca, String modelo, String tipoCasco, String identificacao, int velocidadeMaxima, double preco, int numeroPassageiros, int numeroTripulantes, int numeroCabines, Motor motor2) {
	super.cadastrar(motor1, marca, modelo, tipoCasco, identificacao, velocidadeMaxima, preco, numeroPassageiros);
	this.numeroTripulantes = numeroTripulantes;
	this.numeroCabines = numeroCabines;
	this.motor2 = motor2;	
    }
    
    @Override
    public void entradaDados() {
	System.out.println("Motor1: ");
        super.entradaDados();
	System.out.print("Numero de Tripulantes: "); numeroTripulantes = Integer.parseInt(Config.IN_READ.nextLine());
	System.out.print("Numero de Cabines: ");  numeroCabines = Integer.parseInt(Config.IN_READ.nextLine());
	System.out.println("Motor 2:");
        motor2.entradaDados();
    }
    
    @Override
    public void imprimir() {
	super.imprimir();
	System.out.println("Numero de Tripulantes: " + numeroTripulantes);
	System.out.println("Numero de Cabines: " + numeroCabines);
	motor2.imprimir();
    }
    
    @Override
    public double valorDesconto() {
	return preco * 0.92;
    }
    
    public int pessoasPorCabine() {
	return (numeroPassageiros + numeroTripulantes) / numeroCabines;
    }
    
    //Explicação
    //Iate sempre tem 2 motores.
    //Se o motor extra nao for especificado
    //Significa que o Iate tem 2 motores identicos.
    private void iniciarMotor2ComBaseMotor1() {
	motor2 = new Motor(motor1.getMarca(), motor1.getModelo(), motor1.getTipoCombustivel(), motor1.getPotencia(), motor1.getRpmMaxima(), motor1.getRpm());
    }

    public int getNumeroTripulantes() {
	return numeroTripulantes;
    }

    public int getNumeroCabines() {
	return numeroCabines;
    }

    public Motor getMotor2() {
	return motor2;
    }

    public void setNumeroTripulantes(int numeroTripulantes) {
	this.numeroTripulantes = numeroTripulantes;
    }

    public void setNumeroCabines(int numeroCabines) {
	this.numeroCabines = numeroCabines;
    }

    public void setMotor2(Motor motor2) {
	this.motor2 = motor2;
    }
}
