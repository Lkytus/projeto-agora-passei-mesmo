package net.uva.trabalhoa1;

import java.util.List;
import java.util.ArrayList;
import static net.uva.trabalhoa1.classes.Config.IN_READ;
import net.uva.trabalhoa1.classes.Iate;
import net.uva.trabalhoa1.classes.Jetski;
import net.uva.trabalhoa1.classes.Lancha;
import net.uva.trabalhoa1.classes.Veiculo;

public class Principal {
    private static final List<Veiculo> VEICULOS = new ArrayList<>();
    
    public static void main(String[] args) {
        System.out.println("AEHOU GARAGEM DO CARALHO!");
        while(true) {
            System.out.println("A garagem tem " + VEICULOS.size() + " veiculos");
            System.out.println("O que você quer fazer nessa porra?");
            
            System.out.println("1 - Adicionar mais uma porcaria");
            System.out.println("2 - Ir a merda (via embarcação)");
            System.out.println("3 - Construir");
            System.out.println("4 - Listar");
            
            System.out.print("> ");
            int value = Integer.parseInt(IN_READ.nextLine());
        
            switch(value) {
                case 1:
                    aplicarAdicionarBarco();
                    break;
                case 2:
                    break;
                case 3:
                    break;
                case 4:
                    VEICULOS.stream().forEach((v) -> {
                        System.out.println("-----------------------");
                        v.imprimir();
                    });
                    System.out.println("--------------------------");
                    break;
            }
        }
    }
    
    private static void aplicarAdicionarBarco() {
        System.out.println("Qual tipo de barco você vai adicionar?");
        System.out.println("(Iate, Lancha, Jetski)");
        System.out.print("> ");
        String tipo = IN_READ.nextLine();
        switch(tipo) {
            case "Iate":
                adicionarIate();
                break;
            case "Jetski":
                adicionarJetski();
                break;
            case "Lancha":
                adicionarLancha();
                break;
        }
    }
    private static void adicionarIate() {
        Iate iate = new Iate();
        iate.entradaDados();
        VEICULOS.add(iate);
    }
    
    private static void adicionarJetski() {
        Jetski jet = new Jetski();
        jet.entradaDados();
        VEICULOS.add(jet);
    }
    
    private static void adicionarLancha() {
        Lancha lan = new Lancha();
        lan.entradaDados();
        VEICULOS.add(lan);
    }
    
    
    private static void construirBarco() {
        System.out.println("Ieeeeei");
        System.out.println("Diga o index do barco, pra nos começarmos.");
        System.out.print("> ");
        int index = Integer.parseInt(IN_READ.nextLine());
        Veiculo v = VEICULOS.get(index);
    }
    
    private static void construirLancha(Veiculo vRef) {
        
    }
    
    private static void construirIate(Veiculo vRef) {
        
    }
    
    private static void construirJetski(Veiculo vRef) {
        
    }
}
